# PHP-SFTP-Client

See **lib/SftpClient.php**

This is an SFTP client that utilizes the builtin SSH2 functions
available in PHP.  The goal is to have a single class with all the
methods needed to handle the heavy lifting of *most* connection
scenarios.